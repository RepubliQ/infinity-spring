<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Strict Mode.
    |--------------------------------------------------------------------------
    |
    | Throw an exception when one of the asset dependencies are missing.
    |
    */

    'strict' => false,
];