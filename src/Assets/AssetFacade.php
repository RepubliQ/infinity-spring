<?php

namespace Infinity\Spring\Assets;

use Illuminate\Support\Facades\Facade;

class AssetFacade extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     *
     * @throws \RuntimeException
     */
    protected static function getFacadeAccessor()
    {
        return 'asset';
    }

}