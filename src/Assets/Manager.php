<?php

namespace Infinity\Spring\Assets;

use Illuminate\Support\Arr;
use Infinity\Spring\Assets\Resources\Collection;
use Infinity\Spring\Assets\Resources\LinkResource;
use Infinity\Spring\Assets\Resources\Resource;
use Infinity\Spring\Assets\Resources\ScriptResource;
use Infinity\Spring\Exceptions\MissingCollectionException;
use Infinity\Spring\Exceptions\MissingDependencyException;

class Manager
{
    /**
     * Resource collections.
     *
     * @type \Infinity\Spring\Assets\Resources\Collection[]
     */
    protected $collections = [];

    /**
     * Auto loaded resources.
     *
     * @type \Infinity\Spring\Assets\Resources\Resource[]
     */
    protected $autoload = [];

    /**
     * Strict mode.
     *
     * @type bool
     */
    protected $strict = false;

    /**
     * Collections sorted.
     *
     * @type bool
     */
    protected $sorted = false;

    /**
     * Cached dependencies.
     *
     * @type array
     */
    protected $cachedDependencies = [];

    /**
     * Manager constructor.
     *
     * @param bool $strict Throw exceptions if dependency not found.
     */
    public function __construct($strict = false)
    {
        $this->strict = $strict;
    }

    /**
     * Output scripts.
     *
     * @param string|array|null $collections
     * @return string
     */
    public function scripts($collections = null)
    {
        $output = $this->outputResources('scripts', $collections);

        foreach ($this->autoload as $resource) {
            if ($resource instanceof ScriptResource) {
                $output .= $resource->render();
            }
        }

        return $output;
    }

    /**
     * Output links.
     *
     * @param string|array|null $collections
     * @return string
     */
    public function links($collections = null)
    {
        $output = $this->outputResources('links', $collections);

        foreach ($this->autoload as $resource) {
            if ($resource instanceof LinkResource) {
                $output .= $resource->render();
            }
        }

        return $output;
    }

    /**
     * Get collection.
     *
     * @param string|array $name
     * @return \Infinity\Spring\Assets\Resources\Collection|null
     */
    public function collection($name)
    {
        if (is_array($name)) {
            $collection = [];

            foreach ($name as $colName) {
                $collection[$colName] = $this->collection($colName);
            }

            return $collection;
        }

        foreach ($this->collections as $collection) {
            if ($collection->getName() === $name) {
                return $collection;
            }
        }

        return null;
    }

    /**
     * Get collection index.
     *
     * @param $name
     * @return int|null|string
     */
    protected function collectionIndex($name)
    {
        foreach ($this->collections as $index => $collection) {
            if ($collection->getName() === $name) {
                return $index;
            }
        }

        return null;
    }

    /**
     * Add resources.
     *
     * @param array|\Infinity\Spring\Assets\Resources\Collection|\Infinity\Spring\Assets\Resources\Resource $resource
     */
    public function add($resource)
    {
        if (is_array($resource)) {
            foreach ($resource as $res) {
                $this->add($res);
            }
        } elseif ($resource instanceof Collection) {
            $this->collections[] = $resource;
        } elseif ($resource instanceof Resource) {
            $this->autoload[] = $resource;
        }

        $this->resetSort();
    }

    /**
     * Add resource to collection.
     *
     * @param $name
     * @param \Infinity\Spring\Assets\Resources\Resource $resource
     */
    public function addToCollection($name, Resource $resource)
    {
        $collection = $this->collection($name);

        if (!$collection) {
            $collection = new Collection($name);
            $this->collections[] = $collection;
        }

        $collection->add($resource);

        $this->resetSort();
    }

    /**
     * Remove collection.
     *
     * @param $name
     */
    public function removeCollection($name)
    {
        unset($this->collections[$this->collectionIndex($name)]);

        $this->resetSort();
    }

    /**
     * Clear all resources and collections.
     */
    public function clear()
    {
        $this->collections = [];
        $this->autoload = [];

        $this->resetSort();
    }

    /**
     * Reset sorted state.
     */
    protected function resetSort()
    {
        $this->sorted = false;
    }

    /**
     * Output resource code.
     *
     * @param $type
     * @param null $collections
     * @return string
     * @throws MissingCollectionException
     * @throws MissingDependencyException
     */
    protected function outputResources($type, $collections = null)
    {
        $list = null;

        if ($collections) {
            $list = array_merge((array)$collections, $this->allDependencies($collections, $this->sorted));
        }

        $this->sortByDependency();

        $output = '';

        foreach ($this->collections as $collection) {

            if ($this->strict) {
                $this->hasAllDependencies($collection->getDependencies(), true);
            }

            if ($list) {
                if (in_array($collection->getName(), $list, true)) {
                    $output .= $collection->{$type}();
                }
            } else {
                $output .= $collection->{$type}();
            }
        }

        return $output;
    }

    /**
     * Sort dependencies by theirs load order.
     */
    protected function sortByDependency()
    {
        if ($this->sorted) {
            return;
        }

        $index = 0;
        $decorate = $this->collections;

        array_walk($decorate, function (&$v, $k) use (&$index) {
            $v = [$index++, $k, $v];
        });

        uasort($decorate, function ($left, $right) {
            $dependencies = $left[2]->getDependencies();

            if (!$dependencies) {
                return 0;
            }

            $leftColName = $left[2]->getName();
            $rightColName = $right[2]->getName();

            if ($this->dependsOn($right[2], $leftColName) && ($ri = $right[0] > $li = $left[0])) {
                $right[0] = $li;
                $left[0] = $ri;
            } elseif ($this->dependsOn($left[2], $rightColName) && ($ri = $right[0] < $li = $left[0])) {
                $right[0] = $li;
                $left[0] = $ri;
            }

            return 0;
        });

        $undecorate = [];
        $size = count($decorate);

        for ($i = 0; $i < $size; $i++) {
            $set = Arr::first($decorate, function ($key, $value) use ($i) {
                if ($i === $value[0]) {
                    return true;
                }
            });

            $undecorate[$set[1]] = $set[2];
        }

        $this->sorted = true;

        return $this->collections = $undecorate;
    }

    /**
     * Check if collection depends on another collection.
     *
     * @param \Infinity\Spring\Assets\Resources\Collection $collection
     * @param $dep
     * @return bool
     */
    protected function dependsOn(Collection $collection, $dep)
    {
        $deps = $collection->getDependencies();

        if ($deps) {

            if (in_array($dep, $deps, true)) {
                return true;
            }

            $allDeps = $this->allDependencies($collection, $this->sorted);

            return in_array($dep, $allDeps, true);
        }

        return false;
    }

    /**
     * Get all dependencies for collection.
     *
     * @param $collection
     * @param bool|false $recache
     * @return array
     */
    protected function allDependencies($collection, $recache = false)
    {
        if (!$collection instanceof Collection) {
            $collection = $this->collection($collection);
        }

        if (!$recache && isset($this->cachedDependencies[$collection->getName()])) {
            return $this->cachedDependencies[$collection->getName()];
        }

        $dependencies = $collection->getDependencies();

        if (!$dependencies) {
            return $this->cachedDependencies[$collection->getName()] = $dependencies;
        }

        foreach ($dependencies as $dependency) {
            $dependencies = array_merge($dependencies, $this->allDependencies($dependency));
        }

        return $this->cachedDependencies[$collection->getName()] = array_unique($dependencies);
    }

    /**
     * Check if all dependencies are available.
     *
     * @param $dependencies
     * @param bool $throw
     * @return bool
     * @throws MissingDependencyException
     */
    protected function hasAllDependencies($dependencies, $throw = false)
    {
        foreach ($dependencies as $dependency) {
            if (!$this->collection($dependency)) {
                if ($throw) {
                    throw new MissingDependencyException("Missing {$dependency} dependency.");
                } else {
                    return false;
                }
            }
        }

        return true;
    }
}