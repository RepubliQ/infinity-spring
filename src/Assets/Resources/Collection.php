<?php

namespace Infinity\Spring\Assets\Resources;

class Collection
{
    /**
     * Collection name.
     *
     * @type string
     */
    protected $name;

    /**
     * Link resources.
     *
     * @type \Infinity\Spring\Assets\Resources\LinkResource[]
     */
    protected $links = [];

    /**
     * Script resources.
     *
     * @type \Infinity\Spring\Assets\Resources\ScriptResource[]
     */
    protected $scripts = [];

    /**
     * Dependencies.
     *
     * @type array
     */
    protected $dependencies;

    /**
     * Collection constructor.
     * @param $name
     * @param array $dependencies
     * @param array $resources
     */
    public function __construct($name, array $dependencies = [], array $resources = null)
    {
        $this->name = $name;
        $this->dependencies = $dependencies;

        if ($resources) {
            $this->add($resources);
        }
    }

    /**
     * Add resource.
     *
     * @param \Infinity\Spring\Assets\Resources\Resource|array $resource
     */
    public function add($resource)
    {
        if (is_array($resource)) {
            foreach ($resource as $res) {
                $this->add($res);
            }
        } elseif ($resource instanceof LinkResource) {
            $this->links[] = $resource;
        } elseif ($resource instanceof ScriptResource) {
            $this->scripts[] = $resource;
        }
    }

    /**
     * Output script tags.
     *
     * @return string
     */
    public function scripts()
    {
        $output = '';

        foreach ($this->scripts as $script) {
            $output .= "{$script->render()}\n";
        }

        return $output;
    }

    /**
     * Output link tags.
     *
     * @return string
     */
    public function links()
    {
        $output = '';

        foreach ($this->links as $link) {
            $output .= "{$link->render()}\n";
        }

        return $output;
    }

    /**
     * Get collection name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Get dependencies.
     *
     * @return array
     */
    public function getDependencies()
    {
        return $this->dependencies;
    }

}