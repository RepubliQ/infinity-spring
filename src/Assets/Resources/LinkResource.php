<?php

namespace Infinity\Spring\Assets\Resources;

class LinkResource extends Resource
{
    /**
     * Href.
     *
     * @type string
     */
    protected $href;

    /**
     * LinkResource constructor.
     * @param $href
     * @param $attributes
     */
    public function __construct($href, $attributes)
    {
        $this->href = $href;
        $this->attributes = $attributes;
    }

    /**
     * Css link resource.
     *
     * @param $href
     * @param string $type
     * @param string $rel
     * @return static
     */
    public static function css($href, $type = 'text/css', $rel = 'stylesheet')
    {
        return new static($href, compact('type', 'rel'));
    }

    /**
     * Get the evaluated contents of the object.
     *
     * @return string
     */
    public function render()
    {
        return "<link href=\"{$this->href}\" {$this->getAttributesCode()}/>";
    }
}