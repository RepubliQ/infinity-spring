<?php

namespace Infinity\Spring\Assets\Resources;

use Illuminate\Contracts\Support\Renderable;

abstract class Resource implements Renderable
{
    /**
     * Attributes.
     *
     * @type array
     */
    protected $attributes = [];

    /**
     * Get attributes code.
     *
     * @return string
     */
    protected function getAttributesCode()
    {
        $output = '';

        foreach ($this->attributes as $key => $value) {
            $output .= " {$key}=\"$value\" ";
        }

        return trim($output);
    }

    /**
     * Get attributes.
     *
     * @return array
     */
    public function attributes()
    {
        return $this->attributes;
    }

    /**
     * Set attribute value.
     *
     * @param $name
     * @param $value
     */
    public function setAttribute($name, $value)
    {
        $this->attributes[$name] = $value;
    }
}