<?php

namespace Infinity\Spring\Assets\Resources;

class ScriptResource extends Resource
{
    /**
     * Href.
     *
     * @type string
     */
    protected $href;

    /**
     * Script content.
     *
     * @type string
     */
    protected $content;

    /**
     * LinkResource constructor.
     * @param $script
     * @param $attributes
     * @param bool $content
     */
    public function __construct($script, $attributes, $content = false)
    {
        if ($content) {
            $this->content = $script;
        } else {
            $this->href = $script;
        }

        $this->attributes = $attributes;
    }

    /**
     * Script resource.
     *
     * @param $href
     * @param array $attributes
     * @param string $type
     * @return static
     */
    public static function js($href, array $attributes = [], $type = 'text/javascript')
    {
        return new static($href, array_merge(compact('type'), $attributes));
    }

    /**
     * Inline script tag.
     *
     * @param $content
     * @param array $attributes
     * @param string $type
     * @return static
     */
    public static function inline($content, array $attributes = [], $type = 'text/javascript')
    {
        return new static($content, array_merge(compact('type'), $attributes), true);
    }

    /**
     * Get the evaluated contents of the object.
     *
     * @return string
     */
    public function render()
    {
        if ($this->content) {
            return "<script {$this->getAttributesCode()}>\n{$this->content}\n</script>";
        } else {
            return "<script src=\"{$this->href}\" {$this->getAttributesCode()}></script>";
        }
    }
}