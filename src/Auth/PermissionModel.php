<?php

namespace Infinity\Spring\Auth;

use Infinity\Spring\Contracts\Auth\Permission;
use Infinity\Spring\Models\SpringModel;

abstract class PermissionModel extends SpringModel implements Permission
{
    /**
     * Get role tag.
     *
     * @return string
     */
    public function getPermissionSlug()
    {
        return $this->getAttribute($this->getPermissionSlugName());
    }

    /**
     * Get role tag column name.
     *
     * @return string
     */
    public function getPermissionSlugName()
    {
        return 'slug';
    }

}