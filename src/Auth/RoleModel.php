<?php

namespace Infinity\Spring\Auth;

use Infinity\Spring\Contracts\Auth\Role;
use Infinity\Spring\Models\SpringModel;

abstract class RoleModel extends SpringModel implements Role
{
    public static function bootHasRoles()
    {
        static::deleting(function (Role $role) {
            return $role->isDeletable();
        });
    }

    /**
     * Is the entry deletable.
     *
     * @return bool
     */
    public function isDeletable()
    {
        return (bool)$this->attributes['deletable'];
    }

    /**
     * Get role tag.
     *
     * @return string
     */
    public function getRoleSlug()
    {
        return $this->getAttribute($this->getRoleSlugName());
    }

    /**
     * Get role tag column name.
     *
     * @return string
     */
    public function getRoleSlugName()
    {
        return 'slug';
    }
}