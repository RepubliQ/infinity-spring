<?php

namespace Infinity\Spring\Auth;

use Infinity\Spring\Contracts\Auth\HasPermissions;
use Infinity\Spring\Contracts\Auth\Permission;
use Infinity\Spring\Contracts\Auth\Role;
use Infinity\Spring\Models\SpringModel;

class UserModel extends SpringModel
{
    /**
     * A list of roles. Array values are inverted for speed.
     *
     * @var array
     */
    protected $roles;

    /**
     * A list of permissions. Array values are inverted for speed.
     *
     * @var array
     */
    protected $permissions;

    /**
     * Is super admin.
     *
     * @var bool
     */
    protected $superRole = false;

    /**
     * Columns containing access modifier.
     *
     * @var array
     */
    protected static $accessColumns = ['frontend', 'backend'];

    /**
     * Has access to parts of application.
     *
     * @var array
     */
    protected $access;

    /**
     * Roles relation.
     *
     * @return \Infinity\Spring\Support\Query\Builder|\Infinity\Spring\Support\Eloquent\Builder
     */
    public function roles()
    {
        return $this->belongsToMany(config('auth.role.model'), $this->table, config('auth.role.foreign'), config('auth.role.other'));
    }

    /**
     * Permissions relation.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasManyThrough
     */
    public function permissions()
    {
        return $this->hasManyThrough(config('auth.permission.model'), 'roles');
    }

    /**
     * Get user roles.
     *
     * @param bool $list
     * @return string[]
     */
    public function getRoles($list = true)
    {
        if ($this->roles === null) {
            $this->initRolesAndPermissions();
        }

        return $list ? array_keys($this->roles) : $this->roles;
    }

    /**
     * Get user permissions.
     *
     * @param bool $list
     * @return array|null
     */
    public function getPermissions($list = true)
    {
        if ($this->permissions === null) {
            $this->initRolesAndPermissions();
        }

        return $list ? array_keys($this->permissions) : $this->permissions;
    }

    /**
     * Check if the user has a role.
     *
     * @param string|Role $role
     * @return bool
     */
    public function hasRole($role)
    {
        if ($this->superRole) {
            return true;
        }

        if ($role instanceof Role) {
            $role = $role->getRoleSlug();
        }
        /** @var string $role */
        return isset($this->getRoles(false)[$role]);
    }

    /**
     * Check if user satisfied all or any of the provided roles.
     *
     * @param Role|string|Role[]|string[]|mixed $roles Role or a list of roles.
     * @param bool $strict Require the user to have all roles.
     * @return bool
     */
    public function hasRoles($roles, $strict = false)
    {
        if ($this->superRole) {
            return true;
        }

        $roles = (array)$roles;

        $satisfied = 0;

        foreach ($roles as $role) {
            if (!$strict && $satisfied > 0) {
                return true;
            } elseif ($this->hasRole($role)) {
                $satisfied++;
            } elseif ($strict) {
                return false;
            }
        }

        return $satisfied === count($roles);
    }

    /**
     * Check if user has permission(s).
     *
     * @param Permission|string|Permission[]|string[]|mixed $permissions Permission or a list of permissions.
     * @param bool $strict Require the user to have all permissions.
     * @return bool
     */
    public function can($permissions, $strict = false)
    {
        if ($this->superRole) {
            return true;
        }

        $permissions = (array) $permissions;

        $satisfied = 0;

        foreach ($permissions as $permission) {
            if (!$strict && $satisfied > 0) {
                return true;
            } elseif ($this->hasPermission($permission)) {
                $satisfied++;
            } elseif ($strict) {
                return false;
            }
        }

        return $satisfied === count($permissions);
    }

    /**
     * Check if user has permission.
     *
     * @param Permission|string $permission
     * @return bool
     */
    public function hasPermission($permission)
    {
        if ($permission instanceof Permission) {
            $permission = $permission->getPermissionSlug();
        }
        /** @var string $permission */
        return isset($this->getPermissions(false)[$permission]);
    }

    /**
     * Initialize roles and permissions.
     */
    protected function initRolesAndPermissions()
    {
        // FIXME fix

        if ($this instanceof HasPermissions) {
            $roleList = $this->roles()->with('roles.permissions')->get()->toArray();
            $this->roles = array_flip(array_column($roleList, config('auth.role.slug', 'slug')));
            $this->permissions = array_flip(array_unique(array_flatten(array_column($roleList, 'permissions'))));
        } else {
            $roleList = $this->roles()->lists(array_merge([config('auth.role.slug', 'slug')]));
            $this->roles = array_flip($roleList);
            $this->permissions = [];
        }
    }
}