<?php

namespace Infinity\Spring\Contracts\Auth;

interface HasPermissions
{
    /**
     * Permissions relation.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function permissions();
}