<?php

namespace Infinity\Spring\Contracts\Auth;

interface Permission
{
    /**
     * Get role.
     *
     * @return Role
     */
    public function role();

    /**
     * Get role tag.
     *
     * @return string
     */
    public function getPermissionSlug();

    /**
     * Get role tag column name.
     *
     * @return string
     */
    public function getPermissionSlugName();
}