<?php

namespace Infinity\Spring\Contracts\Auth;

interface Role
{
    /**
     * Is the entry deletable.
     *
     * @return bool
     */
    public function isDeletable();

    /**
     * Permissions relation.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function roles();

    /**
     * Get role tag.
     *
     * @return string
     */
    public function getRoleSlug();

    /**
     * Get role tag column name.
     *
     * @return string
     */
    public function getRoleSlugName();
}