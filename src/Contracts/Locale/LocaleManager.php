<?php

namespace Infinity\Spring\Contracts\Locale;

use Infinity\Spring\Http\Request;

interface LocaleManager
{
    /**
     * Set application locale.
     *
     * @param string $locale
     */
    public function setLocale($locale);

    /**
     * Get active application locale.
     *
     * @return string
     */
    public function getActiveLocale();

    /**
     * Get locale from request.
     *
     * @param Request $request
     * @param string $pathInfo url path (do not call getPathInfo from request).
     * @return mixed
     */
    public function parseLocale(Request $request, $pathInfo);

    /**
     * Alter request if needed. Could be used to remove locale from request.
     *
     * @param Request $request
     * @param string $pathInfo
     * @param string|null $locale
     * @return string $pathInfo
     */
    public function alterRequest(Request $request, $pathInfo, $locale);
}