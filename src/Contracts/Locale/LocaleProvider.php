<?php

namespace Infinity\Spring\Contracts\Locale;

interface LocaleProvider
{
    /**
     * Return an array of available locales.
     *
     * @return array
     */
    public function availableLocales();

    /**
     * Get currently active locale.
     *
     * @return string
     */
    public function activeLocale();
}