<?php

namespace Infinity\Spring\Controllers;

use Illuminate\Contracts\Validation\ValidationException;
use Illuminate\Database\Eloquent\MassAssignmentException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use Illuminate\Routing\Controller;
use Illuminate\Support\MessageBag;
use Infinity\Spring\Exceptions\ActionException;
use Infinity\Spring\Models\SpringModel;
use Infinity\Spring\Support\Eloquent\Builder;

abstract class SpringController extends Controller
{
    /**
     * Model class for controller to work with.
     *
     * @type SpringModel
     */
    protected $model;

    /**
     * Paginate query list results.
     *
     * @type bool
     */
    protected $paginate = false;

    /**
     * Items per page.
     *
     * @type int
     */
    protected $pageSize = 25;

    /**
     * Field passed via request that specifies the page.
     *
     * @type string
     */
    protected $pageQueryField = 'page';

    /**
     * Error messages.
     *
     * @var \Illuminate\Support\MessageBag
     */
    protected $messages;

    /**
     * Http request.
     *
     * @type Request
     */
    protected $request;

    /**
     * SpringController constructor.
     *
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * GET. Get a list of all models.
     *
     * @param int $page
     * @return mixed
     * @throws \InvalidArgumentException
     */
    public function index($page = 1)
    {
        /** @noinspection ExceptionsAnnotatingAndHandlingInspection */
        $query = $this->model()->newQuery();
        $indexSettings = $this->getSettings($query, __FUNCTION__);

        if ($this->paginate) {
            $this->setPagination($page);
            $result = $query->paginate($this->pageSize);
        } else {
            $result = $query->get();
        }

        return $this->showResult($result, $indexSettings, __FUNCTION__);
    }

    /**
     * GET. Create model instance page data.
     *
     * @return mixed
     */
    public function create()
    {
        $createSettings = $this->getSettings(null, __FUNCTION__);
        /** @noinspection ExceptionsAnnotatingAndHandlingInspection */
        return $this->showResult($this->model()->getDefaults(), $createSettings, __FUNCTION__);
    }

    /**
     * POST. Save a new model instance.
     *
     * @throws ValidationException
     * @throws MassAssignmentException
     * @throws ActionException
     */
    public function store()
    {
        $data = $this->request->all();
        /** @noinspection ExceptionsAnnotatingAndHandlingInspection */
        $model = $this->model();

        if (!$model->safeFill($data)) {
            throw new ValidationException($model->getMessages());
        }

        if (!$model->save()) {
            throw new ActionException('Could not store new model instance.');
        }

        return $this->showResult($model, null, __FUNCTION__);
    }

    /**
     * GET. Preview model instance page data.
     *
     * @param $modelId
     * @return mixed
     * @throws ModelNotFoundException
     */
    public function show($modelId)
    {
        $query = $this->model()->newQuery();
        $editSettings = $this->getSettings($query, __FUNCTION__);
        $model = $query->findOrFail($modelId);

        return $this->showResult($model, $editSettings, __FUNCTION__);
    }

    /**
     * GET. Edit model instance page data.
     *
     * @param $modelId
     * @return mixed
     * @throws ModelNotFoundException
     */
    public function edit($modelId)
    {
        $query = $this->model()->newQuery();
        $editSettings = $this->getSettings($query, __FUNCTION__);
        $model = $query->findOrFail($modelId);

        return $this->showResult($model, $editSettings, __FUNCTION__);
    }

    /**
     * PUT/PATCH. Update existing instance.
     *
     * @param $modelId
     * @return mixed
     * @throws \Infinity\Spring\Exceptions\ActionException
     * @throws ModelNotFoundException
     * @throws MassAssignmentException
     * @throws ValidationException
     */
    public function update($modelId)
    {
        $data = $this->request->all();
        $query = $this->model()->newQuery();
        $updateSettings = $this->getSettings($query, __FUNCTION__);
        /** @var SpringModel $model */
        $model = $query->findOrFail($modelId);

        if (!$model->safeFill($data)) {
            throw new ValidationException($model->getMessages());
        }

        if (!$model->save()) {
            throw new ActionException('Could not store new model instance.');
        }

        return $this->showResult($model, null, __FUNCTION__);
    }

    /**
     * DELETE. Delete existing instance.
     *
     * @param $modelId
     * @return mixed
     * @throws ModelNotFoundException
     * @throws ActionException
     * @throws \Exception
     */
    public function destroy($modelId)
    {
        $model = $this->model($modelId, ['id']);

        if (!$model->delete()) {
            throw new ActionException('Could not delete model.');
        }

        return $this->showResult(static::MODEL_DELETED, null, __FUNCTION__);
    }

    /**
     * Get messages.
     *
     * @return \Illuminate\Support\MessageBag
     */
    protected function getMessages()
    {
        if ($this->messages === null) {
            $this->messages = new MessageBag();
        }

        return $this->messages;
    }

    /**
     * Return model instance.
     *
     * @param int|Builder|null $modelId
     * @param array $columns
     * @return \Infinity\Spring\Models\SpringModel
     * @throws ModelNotFoundException
     */
    protected function model($modelId = null, array $columns = ['*'])
    {
        if ($modelId instanceof Builder) {
            return $modelId->findOrFail($modelId, $columns);
        } elseif ($modelId) {
            return $this->model->findOrFail($modelId, $columns);
        } else {
            return new $this->model;
        }
    }

    /**
     * Get action specific settings from Spring model.
     *
     * @param \Illuminate\Database\Query\Builder|\Infinity\Spring\Support\Eloquent\Builder|null $query Action query if available.
     * @param string $name Action name.
     * @return array|null
     */
    protected function getSettings($query, $name)
    {
        /** @noinspection ExceptionsAnnotatingAndHandlingInspection */
        $settings = $this->model()->springSettings($name, $query, $this->request);

        return $settings;
    }

    /**
     * Apply pagination to the query if needed.
     *
     * @param int $page
     * @param int|null $pageSize
     * @param string|null $pageParam
     * @return bool
     */
    protected function setPagination($page = 1, $pageSize = null, $pageParam = null)
    {
        $pageSize = isset($this->pageSize) ? $this->pageSize : $pageSize;
        $pageParam = isset($this->pageQueryField) ? $this->pageQueryField : $pageParam;

        if (empty($pageSize)) {
            return false;
        }

        $page = $this->fireEvents('page');

        if ($page === null) {

            if (empty($pageParam)) {
                return false;
            }

            $page = $this->request->get($pageParam);
        }

        if ($page === null) {
            return false;
        }

        Paginator::currentPageResolver(function () use ($page) {
            return $page;
        });

        return true;
    }

    /**
     * Return response from result.
     *
     * @param mixed $functionResult
     * @param array|null $settingsResult
     * @param string|null $action action name like index, create, etc..
     * @return mixed
     */
    protected function showResult($functionResult, $settingsResult = null, $action = null)
    {
        $data = [
            'success' => 1,
        ];

        if (in_array($action, ['store', 'update'], true)) {
            return $data;
        }

        $data['data'] = [
            $this->getClassName() => $functionResult,
        ];

        if ($settingsResult) {
            $data = array_merge($data['data'], $settingsResult);
        }

        return $data;
    }

    /**
     * Get class name.
     *
     * @return string
     */
    protected function getClassName()
    {
        $parts = explode('\\', $this->model);
        return array_pop($parts);
    }
}