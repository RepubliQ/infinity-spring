<?php

namespace Infinity\Spring\Exceptions;

/**
 * Exception is the base class for
 * all Exceptions.
 * @link http://php.net/manual/en/class.exception.php
 */
class ActionException extends \Exception
{

}