<?php

namespace Infinity\Spring\Http;

use Infinity\Spring\Contracts\Locale\LocaleManager;

class Request extends \Illuminate\Http\Request
{
    /**
     * Returns the path being requested relative to the executed script.
     *
     * The path info always starts with a /.
     *
     * Suppose this request is instantiated from /mysite on localhost:
     *
     *  * http://localhost/mysite              returns an empty string
     *  * http://localhost/mysite/about        returns '/about'
     *  * http://localhost/mysite/enco%20ded   returns '/enco%20ded'
     *  * http://localhost/mysite/about?var=1  returns '/about'
     *
     * @return string The raw path (i.e. not urldecoded)
     *
     * @api
     */
    public function getPathInfo()
    {
        if (null === $this->pathInfo) {
            $this->pathInfo = $this->preparePathInfo();

            if (!config('app.detect_locale', false)) {
                return $this->pathInfo;
            }
        }

        /** @type LocaleManager $localeManager */
        $localeManager = app(LocaleManager::class);

        if ($localeManager) {
            $resolvedLocale = $localeManager->parseLocale($this, $this->pathInfo);

            if ($resolvedLocale === null) {
                $localeManager->setLocale($localeManager->getActiveLocale());
            } else {
                $localeManager->setLocale($resolvedLocale);
                $this->pathInfo = $localeManager->alterRequest($this, $this->pathInfo, $resolvedLocale);
            }
        }

        return $this->pathInfo;
    }
}