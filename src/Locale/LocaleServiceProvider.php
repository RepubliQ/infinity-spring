<?php

namespace Infinity\Spring\Locale;

use Illuminate\Support\ServiceProvider;
use Infinity\Spring\Contracts\Locale\LocaleManager;
use Infinity\Spring\Locale\Managers\UrlRouteManager;

class LocaleServiceProvider extends ServiceProvider
{
    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(LocaleManager::class, UrlRouteManager::class);
    }
}