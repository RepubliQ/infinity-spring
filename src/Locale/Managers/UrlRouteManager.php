<?php

namespace Infinity\Spring\Locale\Managers;

use Illuminate\Foundation\Application;
use Infinity\Spring\Contracts\Locale\LocaleManager;
use Infinity\Spring\Contracts\Locale\LocaleProvider;
use Infinity\Spring\Http\Request;

class UrlRouteManager implements LocaleManager
{
    /**
     * Application.
     *
     * @type Application
     */
    protected $application;

    /**
     * Locale Provider.
     *
     * @type LocaleProvider
     */
    protected $localeProvider;

    /**
     * UrlRouteManager constructor.
     *
     * @param Application $application
     * @param LocaleProvider $provider
     */
    public function __construct(Application $application, LocaleProvider $provider)
    {
        $this->application = $application;
        $this->localeProvider = $provider;
    }

    /**
     * Set application locale.
     *
     * @param string $locale
     */
    public function setLocale($locale)
    {
        $this->application->setLocale($locale);
    }

    /**
     * Get active application locale.
     *
     * @return string
     */
    public function getActiveLocale()
    {
        return $this->localeProvider->activeLocale();
    }

    /**
     * Get locale from request.
     *
     * @param Request $request
     * @param string $pathInfo url path (do not call getPathInfo from request).
     * @return mixed
     */
    public function parseLocale(Request $request, $pathInfo)
    {
        if (empty($pathInfo)) {
            return null;
        }

        $locales = $this->localeProvider->availableLocales();

        if (empty($locales)) {
            return null;
        }

        if (preg_match('/^\/(' . implode('|', $locales) . ')\//', rtrim($pathInfo, '/') . '/', $matches)) {
            return $matches[1];
        }

        return null;
    }

    /**
     * Alter request if needed. Could be used to remove locale from request.
     *
     * @param Request $request
     * @param string $pathInfo
     * @param string|null $locale
     * @return string $pathInfo
     */
    public function alterRequest(Request $request, $pathInfo, $locale)
    {
        if ($locale === null) {
            return $pathInfo;
        }

        return substr($pathInfo, strlen($locale) + 1);
    }
}