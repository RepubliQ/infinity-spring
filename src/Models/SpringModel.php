<?php

namespace Infinity\Spring\Models;

use Illuminate\Database\Eloquent\MassAssignmentException;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\MessageBag;
use Infinity\Spring\Support\Eloquent\Builder;

/**
 * Class SpringModel
 *
 * @package Infinity\Models
 *
 * @mixin \Infinity\Spring\Support\Eloquent\Builder|\Illuminate\Database\Query\Builder
 */
abstract class SpringModel extends Model
{
    /**
     * Validation errors.
     *
     * @type MessageBag
     */
    protected $validationMessages;

    /**
     * Default values when creating a new model. Values will ve overwritten with provided ones.
     *
     * @type array
     */
    protected static $defaults = [];

    /**
     * Create a new Eloquent model instance.
     *
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct(array_merge(static::$defaults, $attributes));
    }

    /**
     * Save a new model and return the instance.
     *
     * @param  array $attributes
     * @return static
     */
    public static function create(array $attributes = [])
    {
        return parent::create(array_merge(static::$defaults, $attributes));
    }

    /**
     * Create a new Eloquent query builder for the model.
     *
     * @param \Illuminate\Database\Query\Builder $query
     * @return \Infinity\Spring\Support\Eloquent\Builder|static
     */
    public function newEloquentBuilder($query)
    {
        return new Builder($query);
    }

    /**
     * Get a new query builder for the model's table.
     *
     * @return \Infinity\Spring\Support\Eloquent\Builder
     */
    public function newQuery()
    {
        return parent::newQuery();
    }

    /**
     * Define an inverse one-to-one or many relationship.
     *
     * @param  string $related
     * @param  string $foreignKey
     * @param  string $otherKey
     * @param  string $relation
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo|\Infinity\Spring\Support\Query\Builder|\Infinity\Spring\Support\Eloquent\Builder
     */
    public function belongsTo($related, $foreignKey = null, $otherKey = null, $relation = null)
    {
        return parent::belongsTo($related, $foreignKey, $otherKey, $relation);
    }

    /**
     * Define a many-to-many relationship.
     *
     * @param  string $related
     * @param  string $table
     * @param  string $foreignKey
     * @param  string $otherKey
     * @param  string $relation
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany|\Infinity\Spring\Support\Query\Builder|\Infinity\Spring\Support\Eloquent\Builder
     */
    public function belongsToMany($related, $table = null, $foreignKey = null, $otherKey = null, $relation = null)
    {
        return parent::belongsToMany($related, $table, $foreignKey, $otherKey, $relation);
    }

    /**
     * Define a one-to-one relationship.
     *
     * @param  string $related
     * @param  string $foreignKey
     * @param  string $localKey
     * @return \Illuminate\Database\Eloquent\Relations\HasOne|\Infinity\Spring\Support\Query\Builder|\Infinity\Spring\Support\Eloquent\Builder
     */
    public function hasOne($related, $foreignKey = null, $localKey = null)
    {
        return parent::hasOne($related, $foreignKey, $localKey);
    }

    /**
     * Define a one-to-many relationship.
     *
     * @param  string $related
     * @param  string $foreignKey
     * @param  string $localKey
     * @return \Illuminate\Database\Eloquent\Relations\HasMany|\Infinity\Spring\Support\Query\Builder|\Infinity\Spring\Support\Eloquent\Builder
     */
    public function hasMany($related, $foreignKey = null, $localKey = null)
    {
        return parent::hasMany($related, $foreignKey, $localKey);
    }

    /**
     * Validate data before filling the model.
     *
     * @param array $data
     * @return static|bool strictly returns false if the data fails validation.
     * @throws MassAssignmentException
     */
    public function safeFill(array $data)
    {
        if (!$this->validate($data)) {
            return false;
        }

        return $this->fill($data);
    }

    /**
     * Validate data before updating the model.
     *
     * @param array $data
     * @return bool|int
     */
    public function safeUpdate(array $data)
    {
        if ($this->validate($data)) {
            return false;
        }

        return $this->update($data);
    }

    /**
     * Validate data against model create rules.
     *
     * @param array $data
     * @return bool|\Illuminate\Support\MessageBag strictly returns true if data is valid.
     */
    public function validateData(array $data)
    {
        $validator = app('validator')->make($data, static::getCreateRules($data));

        if ($validator->fails()) {
            return $validator->messages();
        }

        return true;
    }

    /**
     * Validate data on model.
     *
     * @param array $data
     * @return bool
     */
    public function validate(array $data)
    {
        $this->validationMessages = null;
        $validator = app('validator')->make($data, $this->exists ? static::getUpdateRules($this, $data) : static::getCreateRules($data));

        if ($validator->fails()) {
            $this->validationMessages = $validator->messages();
            return false;
        }

        return true;
    }

    /**
     * Check if the model data is valid.
     *
     * @return bool
     */
    public function isValid()
    {
        return $this->validate($this->attributes);
    }

    /**
     * Check if the model data is invalid.
     *
     * @return bool
     */
    public function isInvalid()
    {
        return !$this->isValid();
    }

    /**
     * Get validation errors.
     *
     * @return \Illuminate\Support\MessageBag
     */
    public function getMessages()
    {
        if (!$this->validationMessages) {
            $this->validationMessages = new MessageBag();
        }

        return $this->validationMessages;
    }

    /**
     * Get fillable fields.
     *
     * @return array
     */
    public function fillableFields()
    {
        return $this->fillable;
    }


    /**
     * Get model defaults. Defaults are automatically applied on new model creation.
     *
     * @return array
     */
    public static function getDefaults()
    {
        return static::$defaults;
    }

    /**
     * Get create rules for model.
     *
     * @param array|null $data Data that will be used to create the model.
     * @return array
     */
    public static function getCreateRules($data = null)
    {
        return [];
    }

    /**
     * Get update rules for model.
     *
     * @param static $model The model that the update rules are validated against.
     * @param array|null $data Data that will be used to update the model.
     * @return array
     */
    public static function getUpdateRules($model, $data = null)
    {
        return [];
    }

    /**
     * Get spring settings for use with SpringController.
     *
     * @param string $action
     *  The default rest actions are: index, create, store, show, edit, update.
     *  It's also possible to send custom action in controller.
     * @param \Illuminate\Database\Query\Builder|\Infinity\Spring\Support\Eloquent\Builder|null $query
     *  You can modify the query through $query object. Can be null, eg. on create.
     * @param \Illuminate\Http\Request $request
     *  You can get request specific data through $request object.
     * @param \Infinity\Spring\Models\SpringModel $model
     *  You can access existing model through $model. It will only be available on update and edit.
     * @return array
     */
    public static function springSettings($action, $query, Request $request, SpringModel $model = null)
    {
        return [];
    }
}