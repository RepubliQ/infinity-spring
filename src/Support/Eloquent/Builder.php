<?php

namespace Infinity\Spring\Support\Eloquent;

class Builder extends \Illuminate\Database\Eloquent\Builder
{
    /**
     * Parse a list of relations into individuals. This modifier allows for ['relationName' => ['columns']].
     *
     * @param  array $relations
     * @return array
     */
    protected function parseRelations(array $relations)
    {
        foreach ($relations as $name => &$constraints) {
            if (is_array($constraints)) {
                $constraints = function ($query) use ($constraints) {
                    /** @var Builder|\Illuminate\Database\Query\Builder $query */
                    $query->select($constraints);
                };
            }
        }

        return parent::parseRelations($relations);
    }

    /**
     * Set the relationships that should be eager loaded.
     *
     * @param mixed $relations
     * @return \Infinity\Spring\Support\Query\Builder|\Infinity\Spring\Support\Eloquent\Builder
     */
    public function with($relations)
    {
        return parent::with($relations);
    }


}