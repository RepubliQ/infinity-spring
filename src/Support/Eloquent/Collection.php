<?php

namespace Infinity\Spring\Support\Eloquent;

class Collection extends \Illuminate\Database\Eloquent\Collection
{
    public function toHierarchy() {
        $dict = $this->getDictionary();
        // Enforce sorting by $orderColumn setting in Baum\Node instance
        uasort($dict, function($a, $b){
            return ($a->getOrder() >= $b->getOrder()) ? 1 : -1;
        });
        return new \Illuminate\Database\Eloquent\Collection($this->hierarchical($dict));
    }
    protected function hierarchical($result) {
        foreach($result as $key => $node)
            $node->setRelation('children', new \Illuminate\Database\Eloquent\Collection);
        $nestedKeys = array();
        foreach($result as $key => $node) {
            $parentKey = $node->getParentId();
            if ( !is_null($parentKey) && array_key_exists($parentKey, $result) ) {
                $result[$parentKey]->children[] = $node;
                $nestedKeys[] = $node->getKey();
            }
        }
        foreach($nestedKeys as $key)
            unset($result[$key]);
        return $result;
    }
}