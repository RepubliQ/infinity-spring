<?php

namespace Infinity\Spring\Support\Query;

use Illuminate\Database\Query\Builder as QueryBuilder;

class Builder extends QueryBuilder
{
    /**
     * Clear order by.
     *
     * @return $this
     */
    public function clearOrderBy()
    {
        $this->orders = null;

        return $this;
    }


}